<?php

get_header();

if (have_posts()):
   while (have_posts()) :the_post();?>
   
   <article class="post">
      
       <?php the_content();?>
   </article>
   
   <?php
   endwhile;
   
   else :
     echo '<p>Nema postova</p>';
	 
	 endif;
	  
	  
get_footer ();

